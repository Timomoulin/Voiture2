import React, { Component } from 'react';
import {
    Link
  } from "react-router-dom";



export class Constructeur extends Component
{
    constructor(props) {
        super(props);
        if(this.props.data)
        {
        this.state={ infos : this.props.data};
        }
      }

      componentDidMount() {
        if(!this.props.data)
        {
        fetch("http://localhost/VoitureAPI/constructeur.php?id="+this.props.id).then(
          (response)=>{
            return response.json();
          }
          ).then((data)=>
          {
            console.log("Constructeur Fetch = ");
            console.log(data);
            this.setState({infos : data,formValide:true});
          }
          ,(e)=>{console.log("Erreur : fetching Constructeur "+e)})
        }
      }
    
      render() {
        
        if(this.state==null){
          return(
          <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
          </div>)
        }
        else{
       if(this.props.crud==="read")
       {
        return (
              <div className="card m-2 p-1 " style={{width:18+"rem"}}>
              <img className="card-img-top" src={this.state.infos.image} alt="Logo de la marque"/>
              <div className="card-body d-flex flex-column justify-content-even">
                <h5 className="card-title">{this.state.infos.nom}</h5>
                <div className="card-text">La société {this.state.infos.nom} fut fondée en {this.state.infos.creation}.<br/> Elle a pour activité la production de : <ul>{this.state.infos.activites.map((act)=><li key={act.id}>{act.label}</li>)} </ul> </div>
                <div>
                <Link to={`constructeur/`+this.props.id} >
                <button className="btn btn-primary m-1" >Voir les véhicules</button>
                </Link> 
                <br/>
                <button className="btn btn-primary m-1" onClick={this.supprimer} >
                Supprimer
                </button>
                <br/>
                <Link to={`updateConstructeur/`+this.props.id} >
                <button className="btn btn-primary m-1" >
                Modifier
                </button>
                </Link>
                </div>
              </div>
            </div>
           
        );
       }
       else if(this.props.crud==="update")
       {
         return(
           <React.Fragment>
             <h1>Formulaire de modification </h1>
             <ul id="msg">
                <div className="text-danger m-1" id="msgnom"></div>
                <div id="msgcreation" className="text-danger m-1"></div>
                <div id="msgsiegeSocial" className="text-danger m-1"></div>
                <div id="msgimage" className="text-danger m-1"></div>
             </ul>
         <form id="updateForm" method="post" onSubmit={this.modifier}>
         
          <input type="number"  className="form-control" name="id" hidden readOnly value={this.props.id}/>
          <label htmlFor="inputNom">Nom </label> 
          <input id="inputNom" onChange={this.handleChange} className="form-control" name="nom" type="text" defaultValue={this.state.infos.nom}/>
          <label htmlFor="inputCreation">Année de création </label>
          <input id="inputCreation" onChange={this.handleChange} className="form-control" name="creation" type="number" min="1800" defaultValue={this.state.infos.creation} />
          <label htmlFor="inputSiege">Pays</label>
          <input id="inputSiege" onChange={this.handleChange} className="form-control" name="siegeSocial" type="text" defaultValue={this.state.infos.siegeSocial}/>
          <label htmlFor="inputImg">URL image</label>
          <input id="inputImg" onChange={this.handleChange} className="form-control" type="text" name="image" defaultValue={this.state.infos.image}/>
          <button className="btn btn-primary m-2">Test</button>
         </form>
         </React.Fragment>
         )
       }
       else if(this.props.crud==="create")
       {
        return(
          <React.Fragment>
            <h1>Formulaire de modification </h1>
            <ul id="msg">
               <div className="text-danger m-1" id="msgnom"></div>
               <div id="msgcreation" className="text-danger m-1"></div>
               <div id="msgsiegeSocial" className="text-danger m-1"></div>
               <div id="msgimage" className="text-danger m-1"></div>
            </ul>
        <form id="createForm" method="post" onSubmit={this.ajouter}>
        
         
         <label htmlFor="inputNom">Nom </label> 
         <input id="inputNom" onChange={this.handleChange} className="form-control" name="nom" type="text" />
         <label htmlFor="inputCreation">Année de création </label>
         <input id="inputCreation" onChange={this.handleChange} className="form-control" name="creation" type="number" min="1800"  />
         <label htmlFor="inputSiege">Pays</label>
         <input id="inputSiege" onChange={this.handleChange} className="form-control" name="siegeSocial" type="text" />
         <label htmlFor="inputImg">URL image</label>
         <input id="inputImg" onChange={this.handleChange} className="form-control" type="text" name="image"/>
         <button className="btn btn-primary m-2">Ajouter</button>
        </form>
        </React.Fragment>
        )
       }
      }
 
        }
        
        renderVehicules=()=>
        {
          alert(this.props.id)
          //ReactDOM.render(<ListVehicule id={this.props.id} data={this.state.infos.vehicules}/>,document.querySelector('#root'))
        }
     
      handleChange=(event)=>
      {
          let nouvelleValeur=event.target.value; 
          let etatAchanger=event.target.name; 
          let changement=this.state.infos//un objet 
          changement[etatAchanger]=nouvelleValeur; 
          let estValide=true;
          let msg="";
          switch(etatAchanger)
          {
            case"nom":
            if(typeof(nouvelleValeur)!="string"||nouvelleValeur.length<2)
            {
              estValide=false;
              msg+="Le nom est trop court";
            }
            break;
            case"creation":
            if(isNaN(parseInt(nouvelleValeur))||nouvelleValeur>new Date().getFullYear())
            {
              estValide=false;
              msg+="L'année de création est trop grande";
            }
            break;
            case"siegeSocial":
            if(nouvelleValeur.length<=2)
            {
              estValide=false;
              msg+="Le nom de pays est trop court";
            }
            break;
            case"image":
            if(nouvelleValeur.trim().startsWith("http")==false)
            {
              estValide=false;
              msg+="URL image n'est pas une URL";
            }
            break;
          }
          if(estValide)
          {
            document.querySelector("#msg"+etatAchanger).innerHTML="";
            event.target.style.borderColor="green";
            event.target.style.backgroundColor="#88CC88";
          this.setState(changement) 
          this.setState({formValide:true});
          }
          else{
            this.setState({formValide:false});
            event.target.style.backgroundColor="red";
            event.target.style.borderColor="red";
            document.querySelector("#msg"+etatAchanger).innerHTML=msg;
          }
          console.log(this.state);
      }
      supprimer=(event)=>
      {
        if(window.confirm("Supprimer "+this.state.infos.nom))
        fetch(`http://localhost/VoitureAPI//deleteconstructeur.php?id=${this.state.infos.id}`).then(
          (response)=>{
            window.location.reload();
          }
        )
      }
      modifier=(event)=>
      {
        event.preventDefault();
        if(this.state.formValide)
        {
        let dataForm=new FormData(event.target);   
        fetch('http://localhost:80/VoitureAPI/updateconstructeur.php', {
          method: 'POST',
          body: dataForm
        }).then((reponse)=>{
          if(reponse.ok)
          {
            alert("Modification effectuer");
            window.history.go(-2);
          }
          else{
            alert("Erreur ");
          }
        },(error)=>{console.log(error);}
        )
        }
      else{
        alert("Le formulaire n'est pas valide")
        }
      }
      ajouter=(event)=>{
        event.preventDefault();
        if(this.state.formValide)
        {
        let dataForm=new FormData(event.target);   
        fetch('http://localhost:80/VoitureAPI/constructeur.php', {
          method: 'POST',
          body: dataForm
        }).then((reponse)=>{
          if(reponse.ok)
          {
            alert("Création effectuer");
            window.history.go(-2);
          }
          else{
            alert("Erreur ");
          }
        },(error)=>{console.log(error);}
        )
        }
      else{
        alert("Le formulaire n'est pas valide")
        }
      }
}