import React, { Component } from 'react';
import { Constructeur } from "./Constructeur";
import {
  Link
} from "react-router-dom";


export class ListConstructeur extends Component
{
  constructor(props)
  {
    super(props);
    this.state={}
  }
  componentDidMount()
  {
    fetch("http://localhost/VoitureAPI/constructeur.php")
    .then(
      (response)=>response.json()
    ).then(
      (data)=>this.setState({lesConstructeurs:data})
    )
  }
    renderConstructeur(uneMarque){
        return(
          <Constructeur id={uneMarque.id} data={uneMarque} key={uneMarque.id} crud="read"/>
        )
    }
    render()
    {
      if(this.state.lesConstructeurs)
      {
      return (
        <React.Fragment>
        <h1>Les Constructeur</h1>

        <div className="row d-flex justify-content-around" >

          {this.state.lesConstructeurs.map((uneMarque)=>this.renderConstructeur(uneMarque))}
        </div>
        <Link to={`createConstructeur`}>
                <div className="row  align-self-center m-3">
                <button className="btn btn-primary">Ajouter un constructeur</button>
                </div>
        </Link>
        </React.Fragment>

      )
      }
      else{
        return(<div>Chargement</div>)
      }
    }
  
}